import { Hero } from './hero';
 
export const HEROES: Hero[] = [
    { id: 1, name: 'Dara' },
    { id: 2, name: 'Piseth' },
    { id: 3, name: 'Thida' },
];